% mreB filament simulation
% (c)Yingjie Sun, Harvard University, 2015-2018
% contact: yingjiesun@g.harvard.edu

%% User specified parameters for filament simulation
% Assume the width of Bacillus subtilis PY79(WT) cell is 1um and the length is 4um  (in CH media)
CELL_LENGTH = 4; %default; Unit: um
CELL_WIDTH = 1.0; %default; Unit: um
PhysicalPixelSize=0.065; % Pixel size: um (Hamamatsu ORCA-Flash4.0 CMOS with 100X objective)
ExposureTime = 1.0; % UNIT: sec

MovieObj.num_particle = 40; % Particle number
MovieObj.num_monomers = 1;% monomer number. The size of each monomer is 5nm.
Orientation = 0;   %Unit: degrees + or -
Velocity_X= 0.030; % Unit: nm/sec
Velocity_Y = 0;    % Unit: nm/sec

TimePerTurn = (pi*CELL_WIDTH)/Velocity_X;
MovieObj.TotalFrameNumber =  ceil(TimePerTurn/ExposureTime); %assume no delay image aquisition
MovieObj.um_per_px = PhysicalPixelSize;% % CMOS with 100X magnification: 0.065 um

SimulateImageHeight = ceil(CELL_LENGTH/PhysicalPixelSize);
SimulateImageWidth =ceil(CELL_WIDTH/PhysicalPixelSize);
MovieObj.sim_box_size_um = [CELL_LENGTH pi 0];
MovieObj.box_size_px = [SimulateImageHeight SimulateImageWidth 0];

MovieObj.sec_per_frame = ExposureTime; % Unit: sec
MovieObj.exposure = ExposureTime;
MovieObj.time_step = ExposureTime;

%% motion configurations
MovieObj.diff_coeff = 0;
MovieObj.u_convection = [Velocity_Y Velocity_X]; % Unit: um
MovieObj.brightness = 1000;
MovieObj.signal_background = 10;
MovieObj.photobleach_tau = inf;
MovieObj.num_particle= MovieObj.num_particle*MovieObj.num_monomers; 

%% Output file
FileName = 'MreB_Simulation';
data = SimulateParticleMotion(FileName,MovieObj);
im = data.im;
im= im-min(im(:));
ImageDataMaxIntensity = max(im(:)); 
SIMUL_MOVIE_MAX_INTENSITY = 300; % limit the signal
ScaleFactor = ImageDataMaxIntensity/SIMUL_MOVIE_MAX_INTENSITY;
im = im./ScaleFactor;
BASE_LINE_NOISE_AMPLITUDE= 10;
BASE_LINE_NOISE= BASE_LINE_NOISE_AMPLITUDE.*rand(size(im));

im(:,:,:) = double(BASE_LINE_NOISE(:,:,:))+double(im(:,:,:));
%% Save images to MAT file
MatFileName = [FileName '.mat'];
save(MatFileName, 'im');
