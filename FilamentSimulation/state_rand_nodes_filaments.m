function [state] = state_rand_nodes_filaments(o)
% generate initial state of randomly distributed nodes in the simulation box
% c is the number of nodes
% o is the structure of simulation parameters
% (c)Yingjie Sun, Harvard University, 2015-2018
% contact: yingjiesun@g.harvard.edu

c = o.num_particle/ o.num_monomers;
L = o.sim_box_size_um;
L = L(1:o.n_dims); % X and Y

% create random starting positions for every filament

xc = [rand(c,1)*L(1) rand(c,1)*L(2)];

% add a particle to the simulation for every monomer
% space these particles by the monomer length to the right of the starting
% position
% this will only work for flow simulations.

xc = kron(xc,ones(o.num_monomers,1));  % Kronecker tensor product.
addition = repmat((0:o.num_monomers-1)',o.num_particle/ o.num_monomers,1) * o.monomer_length; % Replicate and tile an array.
size(xc)
size(addition)
xc(:,2) = xc(:,2) + addition;

state.x = xc; 
end
% figure
% plot(xc(:,1),xc(:,2),'o')