function [ParticleInfo, NodeVelocityList] = FilamentDensityAnalysis(SingleFrameData,ImagingPara, AnalysisSetting)
% Core function for filament density calculation
% Author: Yingjie Sun, Harvard University, 2016-2018
% Input: kymograph image
% Output: particle counting results

[ImageHeight, ImageWidth] = size(SingleFrameData);% unit: pixel
ExtendedCols = ImagingPara.ExtendedCols; % unit: pixel
Rows = round(ImageWidth/ExtendedCols); % unit: pixel
TimeInterval = ImagingPara.TimeInterval; % UNIT: sec
PixelSize_um  = ImagingPara.PixelSize;% UNIT: um
CellWidth_um = ImagingPara.CurrentCellWidth; % unit: um
CellLength_um = ImagingPara.CurrentCellLength;% unit: um

% Analysis parameters setting
TotalContourLevel = AnalysisSetting.TotalContourLevel;
AREA_THRESHOLD_MIN = AnalysisSetting.AREA_THRESHOLD_MIN; % unit: pixel
AREA_THRESHOLD_MAX = AnalysisSetting.AREA_THRESHOLD_MAX; % unit: pixel
INTENSITY_RATIO_THRESHOLD = AnalysisSetting.INTENSITY_RATIO_THRESHOLD;

N_CONNECTED_COMPONENT = AnalysisSetting.N_CONNECTED_COMPONENT; %4 or 8 connected objects
Y_POS_REG_TOLERANCE =AnalysisSetting.Y_POS_REG_TOLERANCE;
X_POS_REG_TOLERANCE =AnalysisSetting.X_POS_REG_TOLERANCE;
ISOLATED_NODE_PROCESSING =AnalysisSetting.ISOLATED_NODE_PROCESSING;

EXPECTED_MIN_VELOCITY = AnalysisSetting.EXPECTED_MIN_VELOCITY;% unit: nm/sec
EXPECTED_MAX_VELOCITY = AnalysisSetting.EXPECTED_MAX_VELOCITY;% unit: nm/sec
MAX_ROWS_PER_NODE = AnalysisSetting.MAX_ROWS_PER_NODE;

ORIENTATION_TOLERANCE = AnalysisSetting.ORIENTATION_TOLERANCE;
EXPECTED_ORIENTATION_MIN = AnalysisSetting.EXPECTED_ORIENTATION_MIN;
EXPECTED_ORIENTATION_MAX = AnalysisSetting.EXPECTED_ORIENTATION_MAX;
ANALYSIS_TIME_WINDOW_SEC = AnalysisSetting.ANALYSIS_TIME_WINDOW_SEC; % unit: sec; default 120sec

% OUTPUT initialization
ParticleInfo = [];
NodeVelocityList = [];
%% image contour analysis: BinaryImage is calculated based on contour levels
figure(1)
[ContourBasedBinaryImage]= ImageContourAnalysis(SingleFrameData, AnalysisSetting);% display image with contour overlay
h = gcf;
close(h);

ContourBasedBinaryImage2= imfill(ContourBasedBinaryImage,N_CONNECTED_COMPONENT, 'holes'); % two-dimensional eight-connected neighborhood
cc = bwconncomp(ContourBasedBinaryImage2, N_CONNECTED_COMPONENT);   % Compute connected regions and properties
[NodeLabel,TotalNodeNumber] = bwlabel(ContourBasedBinaryImage2,N_CONNECTED_COMPONENT);  %TotalNodeNumber is the number of all candidates after image contour analysis

%% Measure properties of image regions
stats = regionprops(cc,'Centroid','Orientation', 'Area','MajorAxisLength','MinorAxisLength','BoundingBox'); %'BoundingBox','PixelList'); %,'Perimeter');
Centroids = cat(1, stats.Centroid);
OrientationList =cat(1, stats.Orientation);
MajorAxisLengthList = cat(1, stats.MajorAxisLength);
MinorAxisLengthList = cat(1, stats.MinorAxisLength);
AreaList = cat(1, stats.Area);
% Calculate the velocity based on the orientation
OrientBasedNodeAbsVelocity = abs((1000*PixelSize_um)./tand(OrientationList))/TimeInterval; % PixelSize: in nm, not precise

% Calculate the connectivity of objects
NodeConnAdjMatrix=zeros(TotalNodeNumber,TotalNodeNumber); % adjacency matrix
for i =1:(TotalNodeNumber-1)
    Xi= round(Centroids(i,1));
    Yi= round(Centroids(i,2));
    
    if  abs(OrientationList(i))>EXPECTED_ORIENTATION_MIN && abs(OrientationList(i))< EXPECTED_ORIENTATION_MAX
        for j=(i+1):TotalNodeNumber
            Xj=round(Centroids(j,1));
            Yj=round(Centroids(j,2));

            ColDifference = abs(ceil(Xi/ExtendedCols) - ceil(Xj/ExtendedCols));
            RowDifference = abs(Yi-Yj);
            OrientDifference = abs(OrientationList(i)-OrientationList(j));
            if  abs(OrientationList(j))>EXPECTED_ORIENTATION_MIN && abs(OrientationList(j))< EXPECTED_ORIENTATION_MAX
                if abs(ColDifference)>0 && abs(ColDifference)<= X_POS_REG_TOLERANCE && RowDifference <= Y_POS_REG_TOLERANCE
                    if OrientDifference <= ORIENTATION_TOLERANCE
                        NodeConnAdjMatrix(i,j)=1;
                    end
                end
            end
        end
    end
end
NodeConnAdjMatrix = NodeConnAdjMatrix+ transpose(NodeConnAdjMatrix);

%% Use an adjacency matrix to create a graph without weights.
NodeConnectedGraph = graph(NodeConnAdjMatrix >= 1);
NodeConnectedGraphCompBins = conncomp(NodeConnectedGraph,'OutputForm','cell');

ConnectedNodeNumber= length(NodeConnectedGraphCompBins); % connected node number in the graph
ConnNodeListCounter = zeros(ConnectedNodeNumber,1);
MaxObjNumber = 15;
ConnNodeList_ID = zeros(ConnectedNodeNumber,MaxObjNumber);

ConnNodeListCentroidID = [];
ConnNodeListCentroidX = [];
ConnNodeListCentroidY = [];
ConnNodeList_Velocity = [];


ConnectedNodeNumber = 0;
IsolatedNodeNumber = 0;
NodeWithInvalidVelocityCounter = 0;
SplittedNodeCounter = 0;
for i=1:length(NodeConnectedGraphCompBins)
    tmpBinList_ID = NodeConnectedGraphCompBins{i}; % This is local ID
    if length(tmpBinList_ID) == 1
        IsolatedNodeNumber = IsolatedNodeNumber+1;
    end
    
    if length(tmpBinList_ID)>=2
        ConnNodeList_Velocity(end+1) = mean(OrientBasedNodeAbsVelocity(tmpBinList_ID)); % Use the mean velocity of all the connected nodes as the estimated velocity
        CurrentVelocity = mean(OrientBasedNodeAbsVelocity(tmpBinList_ID));
        % Sort node X coordinate
        CentroidXList = Centroids(tmpBinList_ID ,1);
        [SortedCentroidXList, SortIndex] = sort(CentroidXList,'ascend'); %(default) Sorts in ascending order.
        MergedNodeCentroidID = tmpBinList_ID(SortIndex(round(length(tmpBinList_ID)/2)));
        
        X0 = Centroids(MergedNodeCentroidID,1);
        Y0 = Centroids(MergedNodeCentroidID,2);
        
        if CurrentVelocity>= EXPECTED_MIN_VELOCITY  && CurrentVelocity <= EXPECTED_MAX_VELOCITY  % unit: nm/sec
            ConnNodeListCentroidID(end+1) = MergedNodeCentroidID;
            ConnNodeListCentroidX(end+1) = X0;
            ConnNodeListCentroidY(end+1) = Y0;
        end
        
        CentroidX_Distance = round((SortedCentroidXList(end)-SortedCentroidXList(1))/ExtendedCols);
        if CentroidX_Distance >= MAX_ROWS_PER_NODE % % need split from the middle
            SplittedNodeCounter = SplittedNodeCounter+1;
            % Modify the current node
            tempLocalIndex = SortIndex(round(length(tmpBinList_ID)/2)-1);
            MergedNodeCentroidID = tmpBinList_ID(tempLocalIndex);
            X0 = Centroids(MergedNodeCentroidID,1);
            Y0 = Centroids(MergedNodeCentroidID,2);
            if ~isempty(ConnNodeListCentroidID)
                ConnNodeListCentroidID(end) = MergedNodeCentroidID; % modify the exisiting ID
                ConnNodeListCentroidX(end) = X0;
                ConnNodeListCentroidY(end) = Y0;
                ConnNodeList_Velocity(end) = CurrentVelocity;
            else
                ConnNodeListCentroidID(1) = MergedNodeCentroidID; % modify the exisiting ID
                ConnNodeListCentroidX(1) = X0;
                ConnNodeListCentroidY(1) = Y0;
                ConnNodeList_Velocity(1) = CurrentVelocity;
            end
            
            %Add another node
            tempLocalIndex = SortIndex(round(length(tmpBinList_ID)/2)+1);
            MergedNodeCentroidID = tmpBinList_ID(tempLocalIndex);
            X0 = Centroids(MergedNodeCentroidID,1);
            Y0 = Centroids(MergedNodeCentroidID,2);
            if ~isempty(ConnNodeListCentroidID)
                ConnNodeListCentroidID(end+1) = MergedNodeCentroidID;
                ConnNodeListCentroidX(end+1) = X0;
                ConnNodeListCentroidY(end+1) = Y0;
                ConnNodeList_Velocity(end+1) = CurrentVelocity;
            else
                ConnNodeListCentroidID(1) = MergedNodeCentroidID;
                ConnNodeListCentroidX(1) = X0;
                ConnNodeListCentroidY(1) = Y0;
                ConnNodeList_Velocity(1) = CurrentVelocity;
            end
        end
        for j=1:length(tmpBinList_ID)
            ConnNodeListCounter(i) = ConnNodeListCounter(i)+1;
            ConnNodeList_ID(i, ConnNodeListCounter(i)) = tmpBinList_ID(j);
        end
    end
end

FinalConnectedNodeNumber = length(ConnNodeListCentroidID);
FinalConnectedNodeID = ConnNodeListCentroidID;
ConnNodeListCentroid(1:FinalConnectedNodeNumber,1) = ConnNodeListCentroidX;
ConnNodeListCentroid(1:FinalConnectedNodeNumber,2) = ConnNodeListCentroidY;

%% Calculate the particle number within a time window
ParticleListWithinTimeWindow= ConnNodeListCentroid(1:FinalConnectedNodeNumber,2)<= ceil(ANALYSIS_TIME_WINDOW_SEC/TimeInterval); %
ParticleNumberWithinTimeWindow  = sum(ParticleListWithinTimeWindow(:));
ParticleDensity = ParticleNumberWithinTimeWindow/(pi*CellWidth_um*CellLength_um);


%% OUTPUT
if ~isempty(ConnNodeList_Velocity)
    NodeVelocityList = ConnNodeList_Velocity;
end

if FinalConnectedNodeNumber>0
    ParticleInfo.TotalNodeNumber =TotalNodeNumber;
    ParticleInfo.ConnectedNodeNumber = FinalConnectedNodeNumber;
    ParticleInfo.ConnectedNodeID = FinalConnectedNodeID;
    ParticleInfo.ConnNodeListCentroidX = ConnNodeListCentroid(:,1);
    ParticleInfo.ConnNodeListCentroidY = ConnNodeListCentroid(:,2);
    ParticleInfo.ParticleNumberWithinTimeWindow = ParticleNumberWithinTimeWindow;
    ParticleInfo.ParticleDensity = ParticleDensity;
end

