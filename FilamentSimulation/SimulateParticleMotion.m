function data = SimulateParticleMotion(filename, varargin)
    if ~exist('filename','var')
        filename = 'movies/test_movie' ;
    end
    o = struct( ...
        ...%%%%%%%% parameters for Brownian dynamics simulation %%%%%%%%%%%%%%%%
         'BD_type', 'free' ...  % BD simulation type. 'free', 'mdomain', or 'corral'
        , 'n_dims', 2 ...      %  number of dimensions in which the simulation is conducted  
        , 'sim_box_size_um',  [2^4*4*0.16 pi 0] ... % the simulated box is of this size. See code below for actual default value (should be box_size_px*um_per_px)
        , 'num_frames',     1000 ...			% number of frames in the stack produced by the code    
        , 'um_per_px',      0.065 ...			% resolution, microns per pixel (Laura: 0.088)
        , 'sec_per_frame',  3e-1 ...		% seconds per frame (interval)
        , 'exposure',       3e-1 ...    % exposure time (sec)
        , 'time_step',      3e-1 ...   %  time step for BD simulation (sec)
        ...
        , 'diff_coeff',     0 ...              	% diffusion coefficient in micron^2/sec    
        , 'u_convection',   [0 0.028] ...        	% convection velocity in microns/sec
        , 'num_particle',   100 ...                 % number of particles
        , 'bound_condi',    'periodic' ...    % boundary condition, 'periodic' or 'random'
        , 'drift', [0 0] ...
        ...
        ...%%%%%%%% parameters for image generation %%%%%%%%%%%%%%%%%%%%%%%%%%%%
        , 'box_size_px',    [2^4*4 2^4 0] ...  	% size of the images.
        , 'psf_type', 'g' ...            	% only gaussian psf ('g') is currently supported           
        , 'psf_sigma_um',   [0.15/2.355 0.15/2.355 0.1] ...  	% ?? standard deviation of the psf in microns, indepedent for all three directions    %, 'psf_sigma_um',   [0.315833333/2.355 0.315833333/2.355 0.1] ...  	% standard deviation of the psf in microns, indepedent for all three directions 
        , 'renderer', 'points_from_nodes_bug' ...  	%   'lines_from_bonds' or 'points_from_nodes'.
        ...
        , 'brightness',        2e3 ...          %   count rate per particle at the center of detection volume
        , 'signal_background',  0 ...  % background light     
        , 'offset',      96 ...   % camera offset
        , 'readout_noise',         3.19  ...         % dark noise rms(std) 6
        , 'EMgain',            300 ...
        , 'ADCgain',           16.92 ...
        , 'monomer_size',       0.0053 ...
        , 'num_monomers',       50 ...
        , 'monomer_length',       0.0053 ... %        ...
        , 'finer_grid' , 10 ... %%% to more accurately simulate bead position
        );
    o.Pe = o.psf_sigma_um(1) * sqrt(o.u_convection*o.u_convection')/o.diff_coeff;

    o = merge_ops(varargin,o);% merge input parameters


    % w_eff = 2*o.psf_sigma_um(1)*((2.311*2*o.psf_sigma_um(1)/o.um_per_px + 0.06765)^-2+1) ; % correction factor for finite pixel size. see Sisan BPJ 2006.
    w_eff = 2*o.psf_sigma_um(1) ;


    % mobile beads
    if o.num_monomers >1
        state = state_rand_nodes_filaments(o);
    else
    state = state_rand_nodes(o) ; % position of the particles
    end
    
    size(state.x)
    o.num_particle 
    % two components
    if o.num_particle == 1
        o.num_particle_1 = 1;
        o.num_particle_2 = 0;
    else
        o.num_particle_1 = floor(0.5* o.num_particle) ;
        o.num_particle_2 = floor(0.5* o.num_particle) ;
    end
    
    o.num_particle = o.num_particle_1 + o.num_particle_2 ;
    o.u_convection_1 = o.u_convection ;
    o.u_convection_2 = -o.u_convection ;
    o.u_convection = cat(1,repmat(o.u_convection_1, [o.num_particle_1 1]),repmat(o.u_convection_2, [o.num_particle_2 1])) ;
    %
    
    tic
    [logs, state, o] = BD_simul8tr(state, o);
    toc
    %

    % vary brightness
    [im, o] = image_generator4(logs,o);
    
    im = im ./ max(im(:)) * 2^16-1;
    
    if max(im(:))>2^16-1
        disp('Intensity values are over-scale. Images will be saved in the double-precistion format.')
    else
        im = uint16(im) ;
    end

%     save([filename '.mat'],'o','im','logs')  ;
    data.o = o;
    data.im = im;
    data.logs = logs;
    %
    clear logs
 
%     figure(35)
%     imm = mean(im, 3) ;
% %     imshow(imm, 'InitialMagnification', 'fit')
%         axis on
%         colormap(jet)
%         h = colorbar ;
%         caxis auto
%         title('mean intensity','FontSize',15)
    %     format_fig2(2)
%     figure(16)
%     % % imsequence_play(im,.5);
%     imsequence_play(im);
    % hold on
    
end