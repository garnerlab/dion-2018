% Tracking(use ImageJ/TrackMate) based filament counting 
% Input: track XML file
% Output: Tracking based filament readout
% (c)Yingjie Sun, Harvard University, 2016-2018
% contact: yingjiesun@g.harvard.edu


%% user specified parameters
% set the minimum length cutoff for tracks, this must be set to at least 4 for successful tracking and velocity fitting
MIN_TRACK_LENGTH = 6;
% set the maximum length cutoff for tracks,  set maxlength = -1 for no cutoff
MAX_TRACK_LENGTH =-1;%
% set the minimim displacement cutoff for tracks
MIN_DISPLACEMENT =0.2; %UNIT: um set mindisp = -1 for no cutoff  absolute displacement


%% open trackmate track, convert to .mat file if necessary
[FileName, PathName, FileFilterIndex] = uigetfile({ '*.xml','Xml file';'*.*','Any file'},'Open a XML file from TrackMate');
if isequal(FileName,0) || isequal(PathName,0)
    return;
end
TracksXMLFileName=[PathName FileName];


TracksFileName_MAT = [TracksXMLFileName(1:end-4) '.mat'];
CreateTrackMATFile =0;
if ~exist(TracksFileName_MAT, 'file')
    CreateTrackMATFile = 1;
    TracksData = importTrackMateTracks(TracksXMLFileName); %
    save(TracksFileName_MAT,'TracksData');
end

%% open tracks .mat file
if exist(TracksFileName_MAT, 'file')
    FullTrackMatFileName = TracksFileName_MAT;
    OriginalTracksData = open(FullTrackMatFileName);% directly  load
    if isfield(OriginalTracksData, 'TracksData')
        TracksData = OriginalTracksData.TracksData;
        OriginalTrackNumber = length(OriginalTracksData.TracksData);
    end
    
    TracksData = TracksData(cellfun(@(x) size(x,1),TracksData)>=MIN_TRACK_LENGTH);
    if MIN_DISPLACEMENT > -1
        TracksData = TracksData(cellfun(@(x) sum((x(end,2:3)-x(1,2:3)).^2,2),TracksData)>= (MIN_DISPLACEMENT^2));
    end
    TrackingBasedFilamentReadout = length(TracksData);
else
    TrackingBasedFilamentReadout = 0;
end