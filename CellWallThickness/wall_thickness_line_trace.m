numberLal = str2num(thickStruct(numberLol).name(strfind(thickStruct(numberLol).name,'bSW262_')+[7:8]));
%% this section loads in the image using matlab bioformats importer
C5 = bfopen(['/Volumes/Internal HDD/TEM Picture/20180819/bSW262_' num2str(numberLal+1) '.dm3']); %C5 will contain the image in a bioformats structure
omemetadata = C5{1, 4}; %this is where the metadata is stored
image = uint16(C5{1}{1});
image = double(image(11:(end-11),11:(end-11)));
pixelSizeNM = double(omemetadata.getPixelsPhysicalSizeX(0).value())
imshow(image,[mean(image(:))-(std(image(:).*3)) mean(image(:))+(std(image(:).*3))])
%% resize and blur image
resizeFactor = pixelSizeNM;
resizeIm = imresize(image, resizeFactor);
blurredIm = imfilter(resizeIm, fspecial('disk',3));

gaussIm8 = imfilter(resizeIm, fspecial('gaussian',64, 8));
gaussIm2 = imfilter(resizeIm, fspecial('gaussian',64, 4));
[Gx, Gy] = imgradientxy(gaussIm8);

[Gmag8, ~] = imgradient(gaussIm8);
[Gmag2, ~] = imgradient(gaussIm2);

[Gxx, Gxy] = imgradientxy(Gx);
[Gyx, Gyy] = imgradientxy(Gy);

eigVal2 = (Gxx + Gyy - sqrt((Gxx - Gyy).^2 + 4*Gxy.^2))/2;


%set noise thresholds via imcrop on resin section of image
[insideSubThreshCrop, rect] = imcrop(eigVal2,[mean(eigVal2(:))-(std(eigVal2(:).*3)) mean(eigVal2(:))+(std(eigVal2(:).*3))]);
insideSubThresh = (mean(insideSubThreshCrop(:)))-(std(insideSubThreshCrop(:)).*1.5);
insideMagSubThreshCrop = imcrop(Gmag8,rect);
insidegMagSubThresh = (mean(insideMagSubThreshCrop(:)))+(std(insideMagSubThreshCrop(:)).*2);
insideImageCrop = imcrop(resizeIm,rect);
insideImageThresh = (mean(resizeIm(:)))-(std(resizeIm(:)).*1.5);
close();

testEig = (eigVal2 - mean(insideSubThreshCrop(:)))./std(insideSubThreshCrop(:));
testMag = (Gmag8 - mean(insideMagSubThreshCrop(:)))./std(insideMagSubThreshCrop(:));
testIm = (resizeIm - mean(insideImageCrop(:)))./std(insideImageCrop(:));
%draw lines along cell wall
C = {};
for i = 1:200   
    pause();  
    imshow(testIm,[-3 3]);
    hold on 
    if ~isempty(C)
        plot(C{i-1}(:,1), C{i-1}(:,2)) 
    end
    [x, y] = getline;
    C{i} = [x, y]; 
    clear x y
end
%% draw lines perpendicular to cell wall 
tic
len = 40;
clear E D F profile profile2
for i = 1:size(C,2)
    a = C{i}(:,1);
    b = C{i}(:,2);
    diffx = diff(a);
    diffy = diff(b);
    t = zeros(1,length(a)-1);
    for n = 1:length(a)-1 
        t(n+1) = t(n) + sqrt(diffx(n).^2+diffy(n).^2);
    end
    tj = linspace(t(1),t(end),round(t(end)));
    xj = interp1(t,a,tj);
    yj = interp1(t,b,tj);
    for j = 1:(size(xj,2)-1)
        thet = atan2((yj(j) - yj(j+1)),(xj(j) - xj(j+1)));
        xx = [xj(j) + (cos(thet + pi/2).*len), xj(j) + (cos(thet - pi/2).*len)];
        yy = [yj(j) + (sin(thet + pi/2).*len), yj(j) + (sin(thet - pi/2).*len)];
        [xd{i, j}, yd{i, j}, profile(:,j)] = improfile(testMag,xx,yy,len.*2);
        profile2(:,j) = improfile(testEig,xx,yy,len.*2);
        clear xx yy thet 
    end
    D{i} = [xj',yj'];
    E{i} = profile;
    F{i} = profile2;
    clear profile xj yj t tj a b diffx diffy
end

%bin and average 10nm sections
clear meanE meanF
num = 10;
for k = 1:size(E,2)
    for i = 1:(2*(size(E{k},2)./num))
        num1 = i.*(num/2) - ((num/2)-1);
        num2 = num1+num-1;
        if num2 < size(E{k},2)
            meanE{k}(:,i) = mean(E{k}(:,num1:num2),2);
            meanF{k}(:,i) = mean(F{k}(:,num1:num2),2);
        end
        clear lol
    end
end
i
toc
locsB = [];
%find two dark peaks in cell wall (inside and outside of cell wall)
for j = 1:size(E,2)
    for i = 1:size(meanE{j},2)
        workingCurve = meanE{j}(:,i);
        workingCurve2 = -meanF{j}(:,i); 
        %{if ismember(i, 1:10:size(meanE{j},2)) 
        %   	findpeaks(workingCurve2,'MinPeakProminence',0.5,'MinPeakHeight',0,'SortStr','descend','MinPeakDistance',5)
        %    hold on
        %    findpeaks(workingCurve,'MinPeakProminence',0.5,'MinPeakHeight',0,'SortStr','descend','MinPeakDistance',5)
        %    pause();
        %    close;
        %}end
        [~, locs2] = findpeaks(workingCurve2,'MinPeakProminence',0.5,'MinPeakHeight',0,'MinPeakDistance',5);
        [~, locs] = findpeaks(workingCurve,'MinPeakProminence',0.5,'MinPeakHeight',0,'MinPeakDistance',5);
        if size(locs,1)>1 && size(locs2,1)>1
            %locs2 is the far apart
            %locs is the close together
            [~, idx] = sort(abs(locs2-len));
            locsX = locs2(idx);
            locsX = sort(locsX(1:2));
            locsB = (locs > locsX(1) & locs < locsX(2));
            clear locsX idx
        end
        if ~isempty(locsB)
            peakLocs{i,j} = locs(locsB);
            locsB = [];
        end
    end
end
%toss bins that don't have two peaks found
diffLocs = cellfun(@diff, peakLocs,'UniformOutput',0);
sizeList = cellfun(@size, diffLocs,'UniformOutput',0);
sizeList = cat(1,sizeList{:});
sizeList2 = cellfun(@size, diffLocs,'UniformOutput',0);

thicknesses = abs(cat(2,diffLocs{sizeList(:,1)==1}));
%plot all of the data to check quality
imshow(resizeIm,[mean(resizeIm(:))-(std(resizeIm(:)).*3) mean(resizeIm(:))+(std(resizeIm(:)).*3)])
%imshow(Gmag,[meanGMag-stdGMag, meanGMag+stdGMag])
hold on
for j = 1:size(E,2)
    for i = 1:size(sizeList2,1)
        if  sum(sizeList2{i,j})==2
            num1 = i.*(num/2) - ((num/2)-1);
            num2 = num1+num-1;
            meanLocs = round(mean([num1 num2]));
            x1 = [xd{j, meanLocs}(peakLocs{i,j})];
            y1 = [yd{j, meanLocs}(peakLocs{i,j})];
            lineDraw{i,j} = [x1; y1];
            plot(x1, y1);
            hold on
        end
    end
end

[mean(thicknesses).*(pixelSizeNM./resizeFactor) std(thicknesses).*(pixelSizeNM./resizeFactor)]
numberLol = size(thickStruct,2)+1

%imshow(resizeIm,[mean(mean(resizeIm))-1000 mean(mean(resizeIm))+1000])
%measure cell diameter
[x, y] = getline;
diameter = hypot(diff(x), diff(y))
close();
%save data
thickStruct(numberLol).nmthick = thicknesses;
thickStruct(numberLol).pixelSizeNM = pixelSizeNM;
thickStruct(numberLol).image = image;
thickStruct(numberLol).thicknessLines = lineDraw;
thickStruct(numberLol).lines = C;
thickStruct(numberLol).profiles = E;
thickStruct(numberLol).diameter = diameter;
thickStruct(numberLol).name = C5{1}{2};
save('bSW262','bSW262','-v7.3')
thickStruct(numberLol).name