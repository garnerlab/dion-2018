function[ContourBasedBinaryImage, ContourThresholdValueList]= ImageContourAnalysis(SingleFrameData, AnalysisSetting)

% Contour analysis for every single kymograph. 
% Input: kymograph and analysis setting
% Output: contour based binary image and the list of threshold value 
% (c)Yingjie Sun, Harvard University, 2016-2018
% contact: yingjiesun@g.harvard.edu

TotalContourLevel = AnalysisSetting.TotalContourLevel;% TotalContourLevel
AREA_THRESHOLD_MIN = AnalysisSetting.AREA_THRESHOLD_MIN;
AREA_THRESHOLD_MAX = AnalysisSetting.AREA_THRESHOLD_MAX;
INTENSITY_RATIO_THRESHOLD = AnalysisSetting.INTENSITY_RATIO_THRESHOLD;
WHOLE_IMAGE_MEAN = mean(SingleFrameData(:));
INT_THRESHOLD = WHOLE_IMAGE_MEAN*INTENSITY_RATIO_THRESHOLD;

if isempty(SingleFrameData) 
    ContourBasedBinaryImage = [];
    ContourThresholdValueList=[];
    return;
end
ExtendedCols = AnalysisSetting.ExtendedCols;
[ImageHeight, ImageWidth] = size(SingleFrameData);
TotalKymographNumber = ImageWidth/ExtendedCols;
ContourBasedBinaryImage = zeros(ImageHeight,ImageWidth); % Initialization

for TileIndex = 1:TotalKymographNumber
    ColBeginIndex = (TileIndex-1)*ExtendedCols +1;
    ColEndIndex = TileIndex*ExtendedCols;
    Subimage = SingleFrameData(:,ColBeginIndex:ColEndIndex); 
    KymographContourBasedBinaryImage = zeros(ImageHeight,ExtendedCols);
    
    [ImageContourMatrix,ImageContourHandle] = imcontour(Subimage, TotalContourLevel);
    
    %% Initialization of contour analysis
    ContourValue=[];
    PointNumber=[];
    Position=[];
    LevelValue=[];

    k=1; % number of contour level
    i=0;
    while (k+1)<length(ImageContourMatrix)
        i=i+1;
        ContourValue(i)=ImageContourMatrix(1,k);
        PointNumber(i) =ImageContourMatrix(2,k);
        Position(i) = k+1;
        k=k+ImageContourMatrix(2,k)+1;
    end
    
    % Find all the level value in the contour
    ValueBegin=0;
    k=0;
    for i=1: length(ContourValue)
        if ContourValue(i)>ValueBegin
            ValueBegin=ContourValue(i);
            k=k+1;
            LevelValue(k)=ContourValue(i);
            LevelValueIndex(k)=i;
        end
    end

    
    %%  Calculate the number of directionally moving particles
    PeakPosIndexBegin = [];
    PeakPosIndexEnd = [];
    ParticleWidth = [];
    ParticleHeight = [];
    ParticlePolygonArea = [];
    
    if TotalContourLevel>0 && ~isempty(LevelValue)
        FilteredCounter =zeros(1,TotalContourLevel);
        PolygonAreaStatMean = zeros(1,TotalContourLevel);
        PolygonAreaStatMedian = zeros(1,TotalContourLevel);
        
        for ContourLevelIndex = 1:TotalContourLevel 
            ContourThresholdValue= LevelValue(ContourLevelIndex); %Use this value as the threshold
            j=0; 
            
            for i=1:length(ContourValue)
                if ContourValue(i) == ContourThresholdValue
                    j=j+1;
                    Begin=Position(i);
                    End =Begin+PointNumber(i)-1;
                    
                    tmpPolyArea = polyarea(ImageContourMatrix(1,Begin:End ) ,ImageContourMatrix(2,Begin:End ));
                    if tmpPolyArea> AREA_THRESHOLD_MIN % Area threshold
                        FilteredCounter(ContourLevelIndex)=FilteredCounter(ContourLevelIndex)+1;
                        PeakPosIndexBegin(ContourLevelIndex,FilteredCounter(ContourLevelIndex))=Begin;
                        PeakPosIndexEnd(ContourLevelIndex,FilteredCounter(ContourLevelIndex))=End;
                        
                        ParticleWidth(ContourLevelIndex,FilteredCounter(ContourLevelIndex)) = ceil(max(ImageContourMatrix(1,Begin:End )))-floor(min(ImageContourMatrix(1,Begin:End )))+1;
                        ParticleHeight(ContourLevelIndex,FilteredCounter(ContourLevelIndex)) = ceil(max(ImageContourMatrix(2,Begin:End )))-floor(min(ImageContourMatrix(2,Begin:End )))+1;
                        ParticlePolygonArea(ContourLevelIndex,FilteredCounter(ContourLevelIndex)) = tmpPolyArea;
                    end
                end
            end
            if FilteredCounter(ContourLevelIndex) >0
                PolygonAreaList = ParticlePolygonArea(ContourLevelIndex,1:FilteredCounter(ContourLevelIndex));
                PolygonAreaStatMean(ContourLevelIndex) = mean(PolygonAreaList);
                PolygonAreaStatMedian(ContourLevelIndex) = median(PolygonAreaList);
            end
        end
        
        CandidateNumber = 0;       
        % adaptively determine the threshold
        ContourThresholdValueList= [];
        for ContourLevelIndex = 1:TotalContourLevel
            for k=1:FilteredCounter(ContourLevelIndex)
                if ParticlePolygonArea(ContourLevelIndex,k) >= AREA_THRESHOLD_MIN && ParticlePolygonArea(ContourLevelIndex,k) <= AREA_THRESHOLD_MAX
                    Begin = PeakPosIndexBegin(ContourLevelIndex,k);
                    End = PeakPosIndexEnd(ContourLevelIndex,k);
                    X =ImageContourMatrix(1,Begin:End );
                    Y = ImageContourMatrix(2,Begin:End );
                    
                    IntesityList= zeros(length(X),1);
                    for PositionIndex = 1:length(X)
                        INT_Y0 = round(Y(PositionIndex));
                        INT_X0 = round(X(PositionIndex));
                        IntesityList(PositionIndex) = Subimage(INT_Y0,INT_X0 );
                    end
                    IntesityListMean = mean(IntesityList);
                    
                    if IntesityListMean > INT_THRESHOLD  
                        for PositionIndex = 1:length(X)
                            KymographContourBasedBinaryImage(round(Y(PositionIndex)),round(X(PositionIndex))) = 1; 
                        end
                        CandidateNumber = CandidateNumber+1;
                        plot(X,Y); 
                    end
                    ContourThresholdValueList(end+1)= LevelValue(ContourLevelIndex); %Use this value as the threshold
                    
                end
            end
        end
        
    end
    ContourBasedBinaryImage(1:ImageHeight,ColBeginIndex:ColEndIndex) = KymographContourBasedBinaryImage(1:ImageHeight,1:ExtendedCols);
end

% CandidateNumber